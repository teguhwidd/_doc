Sasaran Bisnis
  - CCOS - Matrix Proses Fase 2 (Done - 26 September 2019)
  - CCOS - Eform MyCORE Fase 1 (Done - 07 Oktober 2019)
  - CCOS - OBOX (Done - 23 Oktober 2019)
  - CCOS - Tracking Dokur (target implement Q4)
  - CCOS - Migrasi Service Inquiry & Create Loan Account dari LU2 ke LU 6.2 (target implement Q4) 
  - ICOS - Migrasi Service Inquiry & Create Loan Account dari LU2 ke LU6.2 (target implement Q4)
  - ICOS - Migrasi Service Inquiry Data Pengurus BI dari LU2 ke LU 6.2 (target implement Q4) 
  - CCOS - BAP 30 Bulan (target implement Q4) 
  - CCOS - Eform NyCORE Fase 2(target implement 30 Januari 2020)


Kompetensi Perilaku 1:
  - Integrity 
    
Kompetensi Perilaku 2:  
  - Problem Solving


Kompetensi Teknikal
  Saat ini saya sudah terbiasa dengan aturan serta flow code pada source code aplikasi CCOS yang menggunakan Java Struts, 
  dan juga sudah mulai terbiasa dalam pembuatan Service Rest API CCOS yang menggunakan Java Spring.


Rencana Pengembangan
  Untuk teknikal, dalam meningkatkan kemampuan pada framework struts dan framwork spring saya banyak membaca tutorial di internet
  dan belajar secara langsung kepada atasan saya ko Adrian ketika saya di asign project baru dan belum pernah membuat fitur tersebut.
  Dalam belajar framwork spring  boot saya sudah mencoba membuat service rest API sederhana dengan mengikuti tutorial di internet.
  Rencana pengenbangan saya selanjutnya saya ingin lebih menugasai framework sping boot hingga ketingkat advance, 
  saya ingin mengikuti pelatihan/workshop spring boot jika ada difasilitasi oleh BCA misal di BLI Sentul.

  Untuk kompetensi Integrity, saya sudah belajar terkait teknik memberi feedback yang baik kepada orang lain dengan membaca artikel di internet.
  saya juga sudah mulai memberanikan diri untuk menegur orang lain ketika tindakannya tidak sesuai aturan atau kode etik tanpa menyakiti perasaan
  dengan pemilihan kata yang tepat. (misal. ketika ada rekan kerja yang bermain game melebihi waktu jam istirahat, saya menegur teman-teman dengan cara memberitahu jam
  istirahat sudah habis, dan mengajak teman-teman untuk kembali ke meja kerja)

  Untuk kompetensi Problem Solving, saya banyak belajar dengan cara berdiskusi dengan rekan kerja yang lebih senior seperti Ko Adrian, Ko Suwito dan Ko Nixon
  untuk bisa terus meningkatkan pemahaman aplikasi CCOS. Rencana pengenbangan saya selanjutnya untuk meningkatkan kompetensi problem solving, saya ingin mengikuti
  pelatihan penggunaan aplikasi CCOS agar lebih memahami penggunaan aplikasi CCOS dan nantinya diharapkan saya dapat memberikan solusi yang tuntas kepada user.